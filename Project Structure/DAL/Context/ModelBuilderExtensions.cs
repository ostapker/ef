﻿using Bogus;
using DAL.Entities;
using DAL.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int TEAM_COUNT = 10;
        private const int USER_COUNT = 50;
        private const int PROJECT_COUNT = 100;
        private const int TASK_COUNT = 200;

        private const int MAX_NAME_LEN = 100;
        private const int MAX_PERSON_NAME_LEN = 20;
        private const int MAX_EMAIL_LEN = 50;

        public static void Configure(this ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<ProjectEntity>()
                .HasMany(p => p.Tasks)
                .WithOne(t => t.Project)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TeamEntity>()
                .HasMany(t => t.Users)
                .WithOne(u => u.Team)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.Restrict);

            /*modelBuilder.Entity<TeamEntity>()
                .HasMany(t => t.Projects)
                .WithOne(p => p.Team)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.Cascade);*/
            modelBuilder.Entity<ProjectEntity>()
                .HasOne(t => t.Team)
                .WithMany(p => p.Projects)
                .HasForeignKey(p => p.TeamId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<UserEntity>()
                .HasMany(u => u.Tasks)
                .WithOne(t => t.Performer)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.NoAction);

            /* modelBuilder.Entity<UserEntity>()
                 .HasMany(u => u.Projects)
                 .WithOne(p => p.Author)
                 .HasForeignKey(p => p.AuthorId)
                 .OnDelete(DeleteBehavior.Cascade);*/
            modelBuilder.Entity<ProjectEntity>()
                .HasOne(p => p.Author)
                .WithMany(t => t.Projects)
                .HasForeignKey(t => t.AuthorId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProjectEntity>()
                .Property("Name")
                .HasMaxLength(MAX_NAME_LEN);

            modelBuilder.Entity<TaskEntity>()
                .Property("Name")
                .HasMaxLength(MAX_NAME_LEN);

            modelBuilder.Entity<TeamEntity>()
                .Property("Name")
                .HasMaxLength(MAX_NAME_LEN);

            modelBuilder.Entity<UserEntity>()
                .Property("FirstName")
                .HasMaxLength(MAX_PERSON_NAME_LEN);

            modelBuilder.Entity<UserEntity>()
                .Property("LastName")
                .HasMaxLength(MAX_PERSON_NAME_LEN);

            modelBuilder.Entity<UserEntity>()
                .Property("Email")
                .HasMaxLength(MAX_EMAIL_LEN);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers(teams);
            var projects = GenerateRandomProjects(users, teams);
            var tasks = GenerateRandomTasks(users, projects);

            modelBuilder.Entity<TeamEntity>().HasData(teams);
            modelBuilder.Entity<UserEntity>().HasData(users);
            modelBuilder.Entity<ProjectEntity>().HasData(projects);
            modelBuilder.Entity<TaskEntity>().HasData(tasks);
        }

        public static ICollection<TeamEntity> GenerateRandomTeams()
        {
            int teamId = 1;
            var teamsFake = new Faker<TeamEntity>()
                .RuleFor(pi => pi.Id, f => teamId++)
                .RuleFor(pi => pi.Name, f => f.Company.CompanyName())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now));

            return teamsFake.Generate(TEAM_COUNT);
        }

        public static ICollection<UserEntity> GenerateRandomUsers(ICollection<TeamEntity> teams)
        {
            int userId = 1;
            var usersFake = new Faker<UserEntity>()
                .RuleFor(pi => pi.Id, f => userId++)
                .RuleFor(pi => pi.FirstName, f => f.Person.FirstName)
                .RuleFor(pi => pi.LastName, f => f.Person.LastName)
                .RuleFor(pi => pi.Email, f => f.Person.Email)
                .RuleFor(pi => pi.Birthday, f => f.Date.Between(new DateTime(2000, 1, 1), new DateTime(2019, 12, 31)))
                .RuleFor(pi => pi.RegisteredAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now))
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id);

            return usersFake.Generate(USER_COUNT);
        }

        public static ICollection<ProjectEntity> GenerateRandomProjects(ICollection<UserEntity> users, ICollection<TeamEntity> teams)
        {
            int projectId = 1;
            var projectsFake = new Faker<ProjectEntity>()
                .RuleFor(pi => pi.Id, f => projectId++)
                .RuleFor(pi => pi.Name, f => f.Lorem.Sentence())
                .RuleFor(pi => pi.Description, f => f.Lorem.Sentences())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now))
                .RuleFor(pi => pi.Deadline, f => f.Date.Future(2, DateTime.Now))
                .RuleFor(pi => pi.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id);

            return projectsFake.Generate(PROJECT_COUNT);
        }

        public static ICollection<TaskEntity> GenerateRandomTasks(ICollection<UserEntity> users, ICollection<ProjectEntity> projects)
        {
            int taskId = 1;
            var tasksFake = new Faker<TaskEntity>()
                .RuleFor(pi => pi.Id, f => taskId++)
                .RuleFor(pi => pi.Name, f => f.Lorem.Sentence())
                .RuleFor(pi => pi.Description, f => f.Lorem.Sentences())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2020, 1, 1), DateTime.Now))
                .RuleFor(pi => pi.FinishedAt, f => f.Date.Future(2, DateTime.Now))
                .RuleFor(pi => pi.State, f => f.Random.Enum<TaskState>())
                .RuleFor(pi => pi.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(pi => pi.PerformerId, f => f.PickRandom(users).Id);

            return tasksFake.Generate(TASK_COUNT);
        }
    }
}
