﻿using Common.DTOs.Team;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetTeamsAsync();

        Task<TeamDTO> GetTeamByIdAsync(int id);

        Task<TeamDTO> CreateTeamAsync(TeamCreateDTO teamCreateModel);

        Task<TeamDTO> UpdateTeamAsync(TeamUpdateDTO teamUpdateModel);

        Task DeleteTeamByIdAsync(int id);
    }
}
