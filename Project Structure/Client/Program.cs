﻿using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var facade = new Facade();
            await facade.Start();
        }
    }
}
